# turtle.l #

Turtle-like graphics library for Picolisp 64-bit on Linux.

### What is this repository for? ###

* Draw graphics using familiar Turtle commands in your Picolisp program or REPL.
* You can can save your drawings to disk using `savefromscreen`.
* You can also load textures (with `iconfromfile`) and create them (with `iconfromscreen`) and draw them on the window (with `icon`).
* You can even render text using a TTF font.

### How do I get set up? ###

* Dependencies: SDL2, SDL2_image, SDL2_ttf, OpenGL 2.1.
* demo.l shows what the library can. Run it with `pil demo.l -main`.
* MIT license.

### How do I use it? ###

* Load `turtle.l`
* Open a window with `(startturtle)`. You can optionally set the size and title of the window.
* Draw using Turtle commands: `(square 100.0)`. Get help with `(help 'command)`.
* Drawing commands use scaled values (100.0 instead of 100, for instance), but other commands don't because they operate with pixel values.
* Colors are lists of four RGBA values from 0 to 1.0. There are 16 named default colors. Transparency is fully supported, even with background colors.
* Close the window with `(endturtle)`. All hardware resources allocated are tracked by the library and freed automatically. Quitting PicoLisp with `(bye)` or `CTRL+D` also frees all those resources gracefully.

### How to contribute? ###

* Drop me a mail!

### Who do I talk to? ###

* Alfonso Villen: alfonso.villen@gmail.com
