(load "openGl.l")

(push1 '*Bye '(and *Window (endturtle)))

(setq *SDLlib "libSDL2.so"
      *SDLimg "libSDL2_image.so"
      *SDLttf "libSDL2_ttf.so" )

# define things not at all or not well defined in @lib/openGl.l

(setq GL_TEXTURE_2D_MULTISAMPLE 36949
      GL_RGBA8 32856
      GL_UNPACK_ROW_LENGTH 3314 )

(setq glTexImage2D '(@ (pass native *GlutLib "glTexImage2D" NIL))
      glBindTexture '(@ (pass native *GlutLib "glBindTexture" NIL)) )

(de glTexParameteri @ (pass native *GlutLib "glTexParameteri" NIL))

(de glPixelStorei @ (pass native *GlutLib "glPixelStorei" NIL))

(de glGenTextures @ (pass native *GlutLib "glGenTextures" NIL))

(de glDeleteTextures @ (pass native *GlutLib "glDeleteTextures" NIL))

(de glReadPixels @ (pass native *GlutLib "glReadPixels" NIL))

(de glDrawPixels @ (pass native *GlutLib "glDrawPixels" NIL))

(de glVertex2i @ (pass native *GlutLib "glVertex2i" NIL))

(de glTexEnvi @ (pass native *GlutLib "glTexEnvi" NIL))

(de glGenFramebuffers @ (pass native *GlutLib "glGenFramebuffersEXT" NIL))

# 36160 = GL_FRAMEBUFFER_EXT
(de glBindFramebuffer @ (pass native *GlutLib "glBindFramebufferEXT" NIL 36160))

(de glGenRenderbuffers @ (pass native *GlutLib "glGenRenderbuffersEXT" NIL))

# 36161 = GL_RENDERBUFFER_EXT
(de glBindRenderbuffer @ (pass native *GlutLib "glBindRenderbufferEXT" NIL 36161))

(de glRenderbufferStorageMultisample @ (pass native *GlutLib 
   "glRenderbufferStorageMultisampleEXT" NIL 36161 ) )

# 36064 = GL_COLOR_ATTACHMENT0_EXT
(de glFramebufferRenderbuffer @ (pass native *GlutLib "glFramebufferRenderbufferEXT"
   NIL 36160 36064 36161 ) )

(de glDeleteRenderbuffers @ (pass native *GlutLib "glDeleteRenderbuffersEXT" NIL))

(de glDeleteFramebuffers @ (pass native *GlutLib "glDeleteFramebuffersEXT" NIL))

# return value 36053 = GL_FRAMEBUFFER_COMPLETE_EXT
(de glCheckFramebufferStatus () (native *GlutLib "glCheckFramebufferStatusEXT" 'I 36160))

# 36009 = GL_DRAW_FRAMEBUFFER_EXT, 36008 = GL_READ_FRAMEBUFFER_EXT
(de glBindDrawFramebuffer (N) (native *GlutLib "glBindFramebufferEXT" NIL 36009 N))

(de glBindReadFramebuffer (N) (native *GlutLib "glBindFramebufferEXT" NIL 36008 N))

(de glBlitFramebuffer @ (pass native *GlutLib "glBlitFramebufferEXT" NIL))

# define needed SDL2 interfaces

# first some utilities

# surfinf returns a list of tuples with some information of a surface

#{
   SDL_Surface is:                               Offset from surface address
   Uint32 flags                                 0 (but it is padded to 64bit!)
   SDL_PixelFormat *format                      8
   int w                                        16
   int h                                        20
   int pitch (but it seems to be an Uint16)     24 (but it is padded to 64bit!)
   void *pixels                                 32
   void *userdata                               40
   int locked                                   48
   void *lock_data                              ??
   ...
}#

(de surfinf ("PSurf")
   (mapcar '((Name Offset Type)
      (cons Name (car (struct (+ "PSurf" Offset) Type))) )
      '(flags pxfmt w h pitch pixels)
      (0 8 16 20 24 32)
      '((I) (N) (I) (I) (I) (N)) ) )

#{
   SDL_PixelFormat is:                          Offset from pixel format address
   Uint32 format                                0 (but padded to 64bit!)
   SDL_Palette *palette                         8
   Uint8 BitsPerPixel                           16
   Uint8 BytesPerPixel                          17
   Uint8 padding[2]                             18 (we ignore it)
   Uint32 Rmask                                 20
   Uint32 Gmask                                 24
   Uint32 Bmask                                 28
   Uint32 Amask                                 32
}#

# fmtinf returns a list of tuples with some information of a pixel format

(de unsigned (N)
   (& `(dec (** 2 32)) (+ N `(** 2 32))) )
   
(de fmtinf ("Pxfmt")
   (mapcar '((Name Offset Type IsUInt)
      (cons Name
            (if IsUInt
               (unsigned (car (struct (+ "Pxfmt" Offset) Type)))
               (car (struct (+ "Pxfmt" Offset) Type)) ) ) )
      '(fmtval palette bpp bypp rmask gmask bmask amask)
      (0 8 16 17 20 24 28 32)
      '((I) (N) (B) (B) (I) (I) (I) (I))
      '(T NIL NIL NIL T T T T) ) )

# SDL2 functions

(de sdlInit ("IFlag")
    (native `*SDLlib "SDL_Init" 'I "IFlag") )

(de sdlQuit () (native `*SDLlib "SDL_Quit"))

(de sdlGetWindowSize ("PWin")
    (use (IW IH)
       (native `*SDLlib "SDL_GetWindowSize" NIL "PWin"
           '(IW (4 . I)) '(IH (4 . I)) )
       (list IW IH) ) ) # Returns the width and height of the window in a list

(de sdlSetWindowSize @
    (pass native `*SDLlib "SDL_SetWindowSize" NIL) )

(de sdlCreateWindow ("STitle" "IW" "IH" "IFlag")
    (default "IFlag" 0)
    (let (Pos_undefined (hex "1fff0000")
          "PWin" (native `*SDLlib "SDL_CreateWindow" 'P "STitle"
             Pos_undefined Pos_undefined
             "IW" "IH" (| 4 "IFlag") ) ) # 4 = SDL_WINDOW_SHOWN
        "PWin" ) ) # Returns a pointer to a new window

(de sdlDestroyWindow ("PWin")
    (native `*SDLlib "SDL_DestroyWindow" NIL "PWin") )

(de sdlGetError ()
    (native `*SDLlib "SDL_GetError" 'S ) )
    # returns a string

(de sdlFreeSurface ("PSurf")
    (native `*SDLlib "SDL_FreeSurface" NIL "PSurf") )

(de sdlCreateRGBSurfaceFrom ("PPixels" "IW" "IH" "IDepth" "IPitch"
    "IRMask" "IGMask" "IBMask" "IAMask" )
    (native `*SDLlib "SDL_CreateRGBSurfaceFrom" 'P "PPixels" "IW" "IH" "IDepth"
        "IPitch" "IRMask" "IGMask" "IBMask" "IAMask" ) )
        
# convenient function for flipping an SDL_Surface horizontally

(de sdlFlipSurfaceVert ("PSurf")
    (let (Inf (surfinf "PSurf")
          RowLength (; Inf pitch)
          Buf (%@ "malloc" 'P RowLength)
          Top (; Inf pixels)
          Bottom (+ Top (* (dec (; Inf h)) RowLength)) )
         (while (< Top Bottom)
             (%@ "memcpy" 'P Buf Top RowLength)
             (%@ "memcpy" 'P Top Bottom RowLength)
             (%@ "memcpy" 'P Bottom Buf RowLength)
             (inc 'Top RowLength)
             (dec 'Bottom RowLength) )
         (%@ "free" NIL Buf) ) )
         
# SDL2 constants

(let C 0
    (mapc '((K) (set K C) (inc 'C))
        (quote
            # GL attributes (for sdlGlSetAttribute etc.)
            SDL_GL_RED_SIZE
            SDL_GL_GREEN_SIZE
            SDL_GL_BLUE_SIZE
            SDL_GL_ALPHA_SIZE
            SDL_GL_BUFFER_SIZE
            SDL_GL_DOUBLEBUFFER
            SDL_GL_DEPTH_SIZE
            SDL_GL_STENCIL_SIZE
            SDL_GL_ACCUM_RED_SIZE
            SDL_GL_ACCUM_GREEN_SIZE
            SDL_GL_ACCUM_BLUE_SIZE
            SDL_GL_ACCUM_ALPHA_SIZE
            SDL_GL_STEREO
            SDL_GL_MULTISAMPLEBUFFERS
            SDL_GL_MULTISAMPLESAMPLES
            SDL_GL_ACCELERATED_VISUAL
            SDL_GL_RETAINED_BACKING
            SDL_GL_CONTEXT_MAJOR_VERSION
            SDL_GL_CONTEXT_MINOR_VERSION
            SDL_GL_CONTEXT_EGL
            SDL_GL_CONTEXT_FLAGS
            SDL_GL_CONTEXT_PROFILE_MASK
            SDL_GL_SHARE_WITH_CURRENT_CONTEXT
            SDL_GL_FRAMEBUFFER_SRGB_CAPABLE
            SDL_GL_CONTEXT_RELEASE_BEHAVIOR
            SDL_GL_CONTEXT_RESET_NOTIFICATION
            SDL_GL_CONTEXT_NO_ERROR ) ) )

(mapc '((K) (set (car K) (hex (cadr K))))
    (quote
        (SDL_INIT_TIMER                         "1"     )
        (SDL_INIT_AUDIO                         "10"    )
        (SDL_INIT_VIDEO                         "20"    )
        (SDL_INIT_JOYSTICK                      "200"   )
        (SDL_INIT_HAPTIC                        "1000"  )
        (SDL_INIT_GAMECONTROLLER                "2000"  )
        (SDL_INIT_EVENTS                        "4000"  )
        (SDL_INIT_NOPARACHUTE                   "100000")
        (SDL_WINDOW_OPENGL                      "2"     )
        (SDL_GL_CONTEXT_PROFILE_CORE            "0001")
        (SDL_GL_CONTEXT_PROFILE_COMPATIBILITY   "0002")
        (SDL_GL_CONTEXT_PROFILE_ES              "0004")
        (SDL_GL_CONTEXT_DEBUG_FLAG              "0001")
        (SDL_GL_CONTEXT_FORWARD_COMPATIBLE_FLAG "0002")
        (SDL_GL_CONTEXT_ROBUST_ACCESS_FLAG      "0004")
        (SDL_GL_CONTEXT_RESET_ISOLATION_FLAG    "0008")
        (SDL_GL_CONTEXT_RELEASE_BEHAVIOR_NONE   "0000")
        (SDL_GL_CONTEXT_RELEASE_BEHAVIOR_FLUSH  "0001")
        (SDL_GL_CONTEXT_RESET_NO_NOTIFICATION   "0000")
        (SDL_GL_CONTEXT_RESET_LOSE_CONTEXT      "0001") ) )

(de sdlGlSetAttribute (IAttr IVal)
    (native `*SDLlib "SDL_GL_SetAttribute" 'I IAttr IVal) )
    # returns 0 (= success) or -1 (= fail)

(de sdlGlGetAttribute (IAttr)
    (use (R V)
      (setq R (native `*SDLlib "SDL_GL_GetAttribute" 'I IAttr '(V (4 . I))) )
      R ) )
    # returns 0 (= success) or -1 (= fail) and the value of the attribute

(de sdlGlCreateContext (PWin)
    (native `*SDLlib "SDL_GL_CreateContext" 'P PWin) )

(de sdlGlMakeCurrent (PWin PContext)
    (native `*SDLlib "SDL_GL_MakeCurrent" 'I PWin PContext) )

(de sdlGlDeleteContext (PContext)
    (native `*SDLlib "SDL_GL_DeleteContext" NIL PContext) )

# Convenient class for OpenGL windows 

(class +OpenGLWindow)

    # in LAttrib you can pass some OpenGL attributes
    # to be handled by SDL upon window creation
    # LAttrib is a list of cons pairs: '((SDL_GL_DOUBLEBUFFER . 1) ...)
    # Flags must be or'ed together and are passed to sdlCreateSimpleWindow
    # The SDL_WINDOW_OPENGL flag is always set
    
    (dm T (STitle IW IH SVersionMajor SVersionMinor LAttrib IFlag)
        (default SVersionMajor 2 SVersionMinor 1 IFlag 0)
        (let R
            (if (>= SVersionMajor 3)
                (= '(0 0 0)
                    (mapcar '((K V) (sdlGlSetAttribute K V))
                        (list `SDL_GL_CONTEXT_MAJOR_VERSION
                              `SDL_GL_CONTEXT_MINOR_VERSION
                              `SDL_GL_CONTEXT_PROFILE_MASK )
                        (list SVersionMajor
                              SVersionMinor
                              `SDL_GL_CONTEXT_PROFILE_CORE) ) )
                (= '(0 0)
                    (mapcar '((K V) (sdlGlSetAttribute K V))
                        (list `SDL_GL_CONTEXT_MAJOR_VERSION
                              `SDL_GL_CONTEXT_MINOR_VERSION )
                        (list SVersionMajor SVersionMinor) ) ) )
            (when R
                (mapc '((A) (sdlGlSetAttribute (val (car A)) (cdr A))) LAttrib)
                (=: win (sdlCreateWindow STitle IW IH
                    (| IFlag `SDL_WINDOW_OPENGL))) ) )
        (ifn (: win)
            (quit (sdlGetError) "Error creating window")            
            (=: size (sdlGetWindowSize (: win)))
            (=: rate 0)
            (=: context (sdlGlCreateContext (: win)))
            (if (=0 (: context))
               (quit (sdlGetError) "Error creating OpenGL context") )
               (sdlGlMakeCurrent (: win) (: context)) ) )

    (dm destroy> ()
        (sdlGlDeleteContext (: context))
        (sdlDestroyWindow (: win)) )

# define interface to SDL2_image

(setq IMG_INIT_JPG 1
      IMG_INIT_PNG 2
      IMG_INIT_TIF 4 )
      
(de imgInit (IFlag)
    (native `*SDLimg "IMG_Init" 'I IFlag) )
    
(de imgQuit () (native `*SDLimg "IMG_Quit"))

(de imgLoad (SFile)
    (let S (native `*SDLimg "IMG_Load" 'P SFile)
        (if (=0 S)
            (quit "imgLoad: Error loading image" SFile)
            S) ) )
    # Returns a pointer to a SDL_Surface
    
(de imgSavePNG (PSurf SFile) (native `*SDLimg "IMG_SavePNG" 'I PSurf SFile))

# define interface to SDL2_ttf

(setq
   TTF_STYLE_NORMAL            0
   TTF_STYLE_BOLD              1
   TTF_STYLE_ITALIC            2
   TTF_STYLE_UNDERLINE         4
   TTF_STYLE_STRIKETHROUGH     8
   TTF_HINTING_NORMAL          0
   TTF_HINTING_LIGHT           1
   TTF_HINTING_MONO            2
   TTF_HINTING_NONE            3 )
   
(de ttfInit () (native `*SDLttf "TTF_Init" 'I))

(de ttfQuit () (native `*SDLttf "TTF_Quit"))

(de ttfOpenFont @
   (or (pass native `*SDLttf "TTF_OpenFont" 'P)
       (quit "Font file not found.") ) )

(de ttfCloseFont (PF)
   (and PF (native `*SDLttf "TTF_CloseFont" NIL @)) )

(de ttfGetFontStyle @
   (let F (pass native `*SDLttf "TTF_GetFontStyle" 'I)
      (make
         (for S '((0 . normal) (1 . bold) (2 . italic) (4 . underline)
                  (8 . strikethrough) )
            (when (= (car S) (& F (car S)))
               (link (cdr S)) ) ) ) ) )

(de ttfSetFontStyle @ (pass native `*SDLttf "TTF_SetFontStyle" NIL))

(de ttfGetFontHinting @ (pass native `*SDLttf "TTF_GetFontHinting" 'I))

(de ttfSetFontHinting @ (pass native `*SDLttf "TTF_SetFontHinting" NIL))

(de ttfFontFaceIsFixedWidth @
   (gt0 (pass native `*SDLttf "TTF_FontFaceIsFixedWidth" 'I)) )

(de ttfGlyphMetrics (PFont Ch)
   (let (B (%@ "malloc" 'P 20)
         M NIL )
      (when (=0 (native `*SDLttf "TTF_GlyphMetrics" 'I PFont Ch
         B (+ B 4) (+ B 8) (+ B 12) (+ B 16) ) )
         (setq M (struct B '(I I I I I)))
         (%@ "free" NIL B)
         M ) ) )

(de ttfSizeUTF8 (PFont SText)
   (use (W H)
      (when (=0 (native `*SDLttf "TTF_SizeUTF8" 'I PFont SText
         '(W (4 . I)) '(H (4 . I)) ) )
         (list W H) ) ) )

# don't forget to call sdlFreeSurface when you're done using the text!
(de ttfRenderUTF8Blended (Font Text Color4)
   (let C 0
      # Convert the Color4 list into an SDL_Color (a 4-byte number)
      (mapc '((V F) (inc 'C (* V F))) Color4 (1 256 65536 16777216))
      (native `*SDLttf "TTF_RenderUTF8_Blended" 'P Font Text C) ) )
      
# color definitions. default colors are opaque - same colors as FMS Logo

(unless *Colors
   (let N 0
      (setq *Colors
         (make
            (for D
               (quote
                  (black      0        0        0        1.0)
                  (blue       0        0        1.0      1.0)
                  (green      0        1.0      0        1.0)
                  (cyan       0        1.0      1.0      1.0)
                  (red        1.0      0        0        1.0)
                  (magenta    1.0      0        1.0      1.0)
                  (yellow     1.0      1.0      0        1.0)
                  (white      1.0      1.0      1.0      1.0)
                  (brown      0.607843 0.376471 0.231373 1.0)
                  (ocre       0.772549 0.533333 0.070588 1.0)
                  (dark-green 0.392157 0.635294 0.250980 1.0)
                  (turquoise  0.470588 0.733333 0.733333 1.0)
                  (tan-brown  1.0      0.584314 0.466667 1.0)
                  (plum       0.564706 0.443137 0.815686 1.0)
                  (orange     1.0      0.639216 0        1.0)
                  (gray       0.717647 0.717647 0.717647 1.0) )
               (def (car D) (cdr D))
               (link (cdr D))
               (inc 'N) ) ) ) ) )

# turtle graphics definitions

# convert color format 0..1.0 into 0..255

(de rgba (L) (mapcar '((V) (*/ 255 V 1.0)) L))

# the pen color can be one of 0..15, a color symbol, or a list of R G B A
# fixed point values between 0 and 1.0

(de pencolor () (; *Window fore))

(de setpencolor (RGBA)
   (cond
      ((lst? RGBA)
         (put *Window 'fore RGBA) )
      ((<= 0 RGBA 15)
         (put *Window 'fore (get *Colors (inc RGBA))) ) )
   (apply glColor4f (; *Window fore)) )

# the screen color can be one of 0..15, a color symbol or an RGBA value

(de background () (; *Window bkgnd))

(de setbackground (RGBA)
   (cond
      ((lst? RGBA)
         (put *Window 'bkgnd RGBA) )
      ((<= 0 RGBA 15)
         (put *Window 'bkgnd (get *Colors (inc RGBA))) ) )
   (apply glClearColor (; *Window bkgnd)) )

# tint a color from 0 (completely black) to 1.0 (completely white)

(de tint (RGBA F)
   (use N
      (make
         (for C (cut 3 'RGBA)
            (setq N (+ C F))
            (link (if (le0 N) 0 (if (> N 1.0) 1.0 N))) )
         (link (pop 'RGBA)) ) ) )

# set transparency of the current pen color

(de penalpha () (last (; *Window fore)))

(de setpenalpha (A)
   (setpencolor (append (head 3 (; *Window fore)) (list A))) )
   
# create the turtle graphics window

# the graphics window is a single-buffered OpenGL 2.1 window created with SDL2,
# it includes an offscreen multisampled framebuffer and a texture for the 
# turtle icon.
# the back framebuffer (*Window -> fb) will be the active framebuffer by 
# default, only the turtle will be drawn onto the main framebuffer (0) directly.

(de startturtle (W H Title)
   (default Title "Turtle Graphics")
   (or *Window
      (prog
         (default W 512 H 512)
         (sdlInit `SDL_INIT_VIDEO)
         (ttfInit)
         (imgInit `IMG_INIT_PNG)
         (setq *Window (new '(+OpenGLWindow) Title W H 2 1
            '((SDL_GL_DOUBLEBUFFER . 0) (SDL_GL_DEPTH_SIZE . 0)
              (SDL_GL_RED_SIZE . 8) (SDL_GL_BLUE_SIZE . 8)
              (SDL_GL_GREEN_SIZE . 8) (SDL_GL_ALPHA_SIZE . 8)
              (SDL_GL_MULTISAMPLEBUFFERS . 0) (SDL_GL_MULTISAMPLESAMPLES . 0) ) ) )
         (setq *WinW W *WinH H)
         (ifn ("create-fb")
            (quit "Could not set up the graphics window properly!")
            ("create-turtle")
            (glViewport 0 0 W H)
            (glMatrixMode GL_PROJECTION)
            (glLoadIdentity)
            (glOrtho 0 (* 1.0 W) (* 1.0 H) 0 -1.0 1.0)
            (glEnable GL_BLEND)
            (glBlendFunc GL_SRC_ALPHA GL_ONE_MINUS_SRC_ALPHA)
            (reset) ) ) ) )

# create an extra, multisampled framebuffer - the drawing will be rendered there

(de "create-fb" ()
   (use (R CA)
      (glGenFramebuffers 1 '(R (4 . I)))
      (put *Window 'fb R)
      (glBindFramebuffer R)
      (glGenRenderbuffers 1 '(CA (4 . I)))
      (put *Window 'fbrb CA)
      (glBindRenderbuffer CA)
      (glRenderbufferStorageMultisample 4 GL_RGBA8 *WinW *WinH )
      (glFramebufferRenderbuffer CA)
      (= 36053 (glCheckFramebufferStatus)) ) )

(de "destroy-fb" ()
   (and (; *Window fb) (glDeleteFramebuffers 1 (cons NIL (4) (- @))))
   (and (; *Window fbrb) (glDeleteRenderbuffers 1 (cons NIL (4) (- @)))) )

# blit the extra framebuffer to the window

(de drawfb ()
   (glFlush)
   (glBindDrawFramebuffer 0)
   (glBindReadFramebuffer (; *Window fb))
   (use E
      (glBlitFramebuffer 0 0 *WinW *WinH 0 0 *WinW *WinH
         GL_COLOR_BUFFER_BIT GL_NEAREST )
      (glFlush)
      (setq E (glGetError))
      (glBindFramebuffer (; *Window fb))
      (when (<> "0" E) (prinl "drawfb error " E)) ) )
   
# close the turtle graphics window

(de endturtle ()
   (while *Icons (destroyicon (car @)))
   ("destroy-font")
   ("destroy-fb")
   (destroy> *Window)
   (wipe '(*Window *WinW *WinH))
   (imgQuit)
   (sdlQuit) )

# display surface info
# this isn't needed, unless for debugging iconfromscreen and savefromscreen
#{
(de surface-info (Text Inf Dump)
   (prinl Text)
   (println 'Surface Inf)
   (let P (fmtinf (get Inf 'pxfmt))
      (println 'PixelFormat P)
      (println 'PixelFormatName (sdlGetPixelFormatName (get P 'fmtval))) )
   (when Dump
      (println 'Dump (struct (get Inf 'pixels)
         (cons 'B (* 4 (get Inf 'w) (get Inf 'h))) ) ) ) )
}#

(de "create-texture" (W H Pixels)
   (use R
      (glGenTextures 1 '(R (4 . I)))
      (glBindTexture GL_TEXTURE_2D R)
      (glTexParameteri GL_TEXTURE_2D GL_TEXTURE_MIN_FILTER GL_LINEAR)
      (glTexParameteri GL_TEXTURE_2D GL_TEXTURE_MAG_FILTER GL_LINEAR)
      # 10242 = 0x2802 = GL_TEXTURE_WRAP_S
      # 33071 = 0x812f = GL_CLAMP_TO_EDGE
      (glTexParameteri GL_TEXTURE_2D 10242 33071)
      # 10243 = 0x2803 = GL_TEXTURE_WRAP_T
      (glTexParameteri GL_TEXTURE_2D 10243 33071)
      # 8960 = 0x2300 = GL_TEXTURE_ENV
      # 8704 = 0x2200 = GL_TEXTURE_ENV_MODE
      # 7681 = 0X1e01 = GL_REPLACE
      (glTexEnvi 8960 8704 7681)
      (glTexImage2D GL_TEXTURE_2D 0 GL_RGBA8 W H 0 GL_RGBA
         GL_UNSIGNED_BYTE Pixels )
      R ) )
      
(de iconfromfile (File)
   # create a texture
   (let (Sur (imgLoad File) # Use SDL2_image to load the image
         Inf (surfinf Sur)             # Get the SDL_SURFACE structure
         Icon ("create-texture" (get Inf 'w) (get Inf 'h) (get Inf 'pixels)) )
      (sdlFreeSurface Sur)
      Icon ) )

# create the turtle icon (a texture) - the turtle can be 8.0, 16.0, 32.0,
# 48.0 or 64.0 pixels big

(de "create-turtle" ()
   (unless (; *Window icon)
      (put *Window 'icon
         (iconfromfile
            `(pack (car (file)) "res/tortoise.png")) )
      (put *Window 'tsize 32.0) ) )

(de iconfit (X Y W H)
   (and
      (< -1 X *WinW)
      (< -1 Y *WinH)
      (<= (+ X W) *WinW)
      (<= (+ Y H) *WinH) ) )

# create an icon (texture) from a region of the screen

(de iconfromscreen (X Y W H)
   (when (iconfit X Y W H)
      (drawfb)
      (let (Buf (%@ "malloc" 'P (* W H 4))
            Icon NIL )
         (glBindFramebuffer 0)
         (glReadPixels X (- *WinH (+ Y H))
            W H GL_RGBA GL_UNSIGNED_BYTE Buf)
         (setq Icon ("create-texture" W H Buf))
         (glBindFramebuffer (; *Window fb))
         (%@ "free" NIL Buf)
         (when (showturtle?) (drawturtle))
         (push '*Icons Icon) ) ) )

# save a region of the screen as a PNG image file

(de savefromscreen (X Y W H SFile)
   (when (iconfit X Y W H)
      (drawfb)
      (let (Buf (%@ "malloc" 'P (* W H 4))
            Showt (showturtle?)
            Surf NIL )
         (hideturtle)
         (glBindFramebuffer 0)
         (glReadPixels X (- *WinH (+ Y H))
            W H GL_RGBA GL_UNSIGNED_BYTE Buf)
         (setq Surf (sdlCreateRGBSurfaceFrom Buf W H 32 (* W 4)
            255 65280 16711680 4278190080 ) )
         (sdlFlipSurfaceVert Surf)
         (imgSavePNG Surf SFile)
         (sdlFreeSurface Surf)
         (%@ "free" NIL Buf)
         (put *Window 'showt Showt)
         (repaint) ) ) )
         
# free an icon texture

(de destroyicon (Icon)
   (glDeleteTextures 1 (cons NIL (4) (- Icon)))
   (del Icon '*Icons) )

# draw the turtle icon - the texture is always rendered in the main window,
# so it appears on top of the drawing, but the drawing (without the turtle icon)
# is rendered in the extra framebuffer

(de "draw-icon" (Icon Sx Sy)
   (glBindTexture GL_TEXTURE_2D Icon)
   (glMatrixMode GL_MODELVIEW)
   (glLoadIdentity)
   (let (Nx (/ Sx 2) Ny (/ Sy 2))
      (glTranslatef (xcor) (ycor) 0)
      (glRotatef (degrees (heading)) 0 0 1.0)
      (glEnable GL_TEXTURE_2D)
      (glBegin GL_QUADS)
      (glTexCoord2f 0 1.0)
      (glVertex2f (- Nx) (- Ny))
      (glTexCoord2f 0 0)
      (glVertex2f (- Nx) Ny)
      (glTexCoord2f 1.0 0)
      (glVertex2f Nx Ny)
      (glTexCoord2f 1.0 1.0)
      (glVertex2f Nx (- Ny))
      (glEnd)
      (glLoadIdentity) )
      (glDisable GL_TEXTURE_2D) )

(de drawturtle ()
   (glBindFramebuffer 0)
   (let S (; *Window tsize)
      ("draw-icon" (; *Window icon) S S) )
   (glFlush)
   (glBindFramebuffer (; *Window fb)) )
      
# create the font for text rendering

(de setlabelheight (Size)
   (when (; *Window font) (ttfCloseFont @))
   (put *Window 'font (ttfOpenFont (pack (; *Window fname) ".ttf") Size))
   (put *Window 'fsize Size)
   (unless (; *Window ftex)
      (use R
         (glGenTextures 1 '(R (4 . I)))
         (put *Window 'ftex R) ) ) )

(de setlabelfont (Name Size)
   (put *Window 'fname Name)
   (setlabelheight Size) )

# destroy the font object and close SDL2_ttf

(de "destroy-font" ()
   (glDeleteTextures 1 (cons NIL (4) (; *Window ftex)))
   (ttfCloseFont (; *Window font))
   (ttfQuit) )

# resize the turtle graphics window

(de resizewindow (W H)
   ("destroy-fb")
   (sdlSetWindowSize (; *Window win) W H )
   (put *Window 'size (list W H))
   (setq *WinW W *WinH H)
   (glViewport 0 0 W H)
   (glMatrixMode GL_PROJECTION)
   (glLoadIdentity)
   (glOrtho 0 (* 1.0 W) (* 1.0 H) 0 -1.0 1.0)
   ("create-fb")
   (reset) )
   
# antialiased lines and polygons

(de smoothon ()
   # 32925 = GL_MULTISAMPLE
   (glEnable 32925) )

# non-antialiased lines and polygons

(de smoothoff ()
   (glDisable 32925) )

# default state

(de reset ()
   (setbackground (1.0 1.0 1.0 1.0))
   (setpencolor (0 0 0 1.0))
   (setpenwidth 1.0)
   (setturtlesize 32.0)
   (setheading 0)
   (smoothon)
   (pendown)
   (clearscreen)
   (showturtle)
   (setlabelfont `(pack (car (file)) "res/DejaVuSans") 16)
   (loop0) )

(de penup () (put *Window 'down NIL))

(de pendown () (put *Window 'down T))

(de setpenwidth (N)
   (put '*Window 'pw N)
   (glLineWidth N) )

# current angle of the turtle - in radians

(de heading () (; *Window angle))

(de degrees (Rad) (*/ 180.0 Rad pi))

# if the pen of the turtle is down

(de down? () (; *Window down))

# the "loop" property avoids unnecessary repainting - "repaint" only draws
# if loop = 0
# the "to" and "rep" functions increase the value of the "loop" property,
# so that the render is done only after the last of those commands has returned

(de loop0 ()
   (put *Window 'loop 0)
   (repaint) )

(de loop+ () (inc (prop *Window 'loop)))

(de loop- ()
   (and (=0 (dec (prop *Window 'loop)))
        (repaint) ) )

(de loop? () (gt0 (; *Window loop)))

(de repaint ()
   (drawfb)
   (when (showturtle?)
      (drawturtle) ) )
         
# turtle coordinates are scaled

(de xcor () (; *Window x))

(de ycor () (; *Window y))

(de pos () (list (xcor) (ycor)))

# check if a filled polygon is being drawn

(de poly? () (; *Window poly))

# define new drawing functions (see examples below) - "to" increases the loop #
# property before the commands, so the drawing is updated after the function
# returns (and loop = 0, meaning that there are no more commands to be executed
# and the turtle is in "immediate mode")

(de to L
   (def (++ L)
      (make
         (link (++ L) '(loop+))
         (chain L)
         (link '(loop-)) ) ) )
         
(to showturtle () (put *Window 'showt T))

(to hideturtle () (put *Window 'showt))

(de showturtle? () (; *Window showt))

(de turtlesize () (; *Window tsize))

(to setturtlesize (N) # size in units (default: 32.0) 
   (when (gt0 N)
      (put *Window 'tsize N) ) )

# draw an icon (a texture) on the screen (the back framebuffer)

(to icon (Icon W H) ("draw-icon" Icon W H))

# place the turtle at the center of the screen, and repaint if it is visible

(to home (Color)
   (goto (*/ 1.0 *WinW 2) (*/ 1.0 *WinH 2) Color) )

(to left (N)
   (dec (prop *Window 'angle) (*/ N pi 180.0)) )

(to right (N)
   (inc (prop *Window 'angle) (*/ N pi 180.0)) )

(to setheading (A)
   (put *Window 'angle (*/ A pi 180.0)) )

# make the turtle point towards a target point

(to towards (X Y)
   (put *Window 'angle (atan2 (- Y (ycor)) (- X (xcor)))) )
         
# goto checks if a filled polygon is being drawn and does different things

(to goto (X Y Color)
   (when (down?)
      (if (poly?)
         (prog
            (and Color (setpencolor Color))
            (glVertex2f X Y) ) # when drawing a filled polygon
         (glBegin GL_LINES) # when drawing single lines
         (glVertex2f (xcor) (ycor))
         (and Color (setpencolor Color))
         (glVertex2f X Y)
         (glEnd) ) )
   (put *Window 'x X)
   (put *Window 'y Y) )

# jumpto is like goto, but it never draws a line

(to jumpto (X Y)
   (put *Window 'x X)
   (put *Window 'y Y) )

(de forward (N Color)
   (goto (+ (xcor) (*/ N (cos (heading)) 1.0))
         (+ (ycor) (*/ N (sin (heading)) 1.0))
         Color ) )
         
(de back (N Color) (forward (- N) Color))

(de setx (X Color) (goto X (ycor) Color))

(de sety (Y Color) (goto (xcor) Y Color))

# clears the screen

(to clean ()
   (glClear GL_COLOR_BUFFER_BIT) )

(to clearscreen ()
   (jumpto (*/ 1.0 *WinW 2) (*/ 1.0 *WinH 2))
   (clean) )

# pget and pset are used for flood filling
# screen coordinates (pget / pset) are not scaled

(de pget (P XY)
   (glReadPixels (car XY) (cdr XY) 1 1 GL_RGB GL_UNSIGNED_BYTE P)
   (struct P '(B . 3)) )

(de pset (L)
   (glBegin GL_POINTS)
   (for XY L (glVertex2i (car XY) (cdr XY)))
   (glEnd) )
   
(de visible? (XY)
   (and
      (< -1 (car XY) *WinW)
      (< -1 (cdr XY) *WinH)
      XY ) )

# start a flood fill at the current position of the turtle using the current color;
# pixels are read from the main window and drawn in the extra framebuffer

# two stacks are used: Check stores the pixels to be checked, Paint holds the
# pixels to be painted

(to floodfill ()
   (drawfb)
   (glBindFramebuffer 0)
   (let (B (%@ "malloc" 'P 8) # off 0 = src, 4 = get
         Src NIL
         Get (+ B 4)
         Spos (cons (/ (xcor) 1.0)
              (- *WinH (/ (ycor) 1.0)) )
         Check (and (visible? Spos) (list @)) # start only if XY inside the screen
         Paint NIL
         N NIL )
      (when Check
         (setq Src (pget B Spos)) # store source color
         (while (setq N (++ Check)) # check pixels
            (when
               (and
                  (not (member N Paint))
                  (= Src (pget Get N)) )
               (push 'Paint N)
               (and (visible? (cons (inc (car N)) (cdr N))) (push 'Check @))
               (and (visible? (cons (dec (car N)) (cdr N))) (push 'Check @))
               (and (visible? (cons (car N) (inc (cdr N)))) (push 'Check @))
               (and (visible? (cons (car N) (dec (cdr N)))) (push 'Check @)) ) )
         (glBindFramebuffer (; *Window fb))
         (glMatrixMode GL_PROJECTION)
         (glPushMatrix)
         (glLoadIdentity)
         (glOrtho 0 (* 1.0 *WinW)
            0 (* 1.0 *WinH) -1.0 1.0 )
         (pset Paint)
         (glPopMatrix)
         (glMatrixMode GL_MODELVIEW)
         (%@ "free" NIL B) ) ) )

(to rep (N . Prg)
   (for Repcount N
      (run Prg) ) )

# draw a filled polygon

(to filled Prg
   (put *Window 'poly T)
   (glBegin GL_POLYGON)
   (apply glVertex2f (pos))
   (run Prg)
   (glEnd)
   (put *Window 'poly) )

# render some text with the current font and the current font size at the
# current position and angle of the turtle

(to label @
   (use (Color Sur Inf)
      (setq Color (rgba (; *Window fore)))
      (setq Sur (ttfRenderUTF8Blended (; *Window font)
         (pack (rest))
         Color ) )
      (setq Inf (surfinf Sur))             # Get the SDL_SURFACE structure
      (glBindTexture GL_TEXTURE_2D (; *Window ftex))
      (glTexParameteri GL_TEXTURE_2D GL_TEXTURE_MIN_FILTER GL_LINEAR)
      (glTexParameteri GL_TEXTURE_2D GL_TEXTURE_MAG_FILTER GL_LINEAR)
      (glTexEnvi 8960 8704 7681)
      (glPixelStorei GL_UNPACK_ROW_LENGTH (/ (get Inf 'pitch) 4))
      (glTexImage2D GL_TEXTURE_2D 0 GL_RGBA8 (get Inf 'w) (get Inf 'h)
         0 GL_BGRA GL_UNSIGNED_BYTE (get Inf 'pixels) )
      (glPixelStorei GL_UNPACK_ROW_LENGTH 0)
      (sdlFreeSurface Sur)
      (glMatrixMode GL_MODELVIEW)
      (glLoadIdentity)
      (glTranslatef (xcor) (ycor) 0)
      (glRotatef (degrees (heading)) 0 0 1.0)
      (glEnable GL_TEXTURE_2D)
      (glBegin GL_QUADS)
      (glTexCoord2f 0 0)
      (glVertex2f 0 0)
      (glTexCoord2f 0 1.0)
      (glVertex2f 0 (* 1.0 (get Inf 'h)))
      (glTexCoord2f 1.0 1.0)
      (glVertex2f (* 1.0 (get Inf 'w)) (* 1.0 (get Inf 'h)))
      (glTexCoord2f 1.0 0)
      (glVertex2f (* 1.0 (get Inf 'w)) 0)
      (glEnd)
      (glDisable GL_TEXTURE_2D)
      (glLoadIdentity)
      (let Down (down?)
         (right 90.0)
         (penup)
         (forward (* 1.0 (get Inf 'h)))
         (left 90.0)
         (and Down (pendown)) ) ) )

# --------------------------------

# Utilities

# Size, SizeX, SizeY, Width = scaled numbers
# LR = left | right (can be omitted)
# Colors = list of color indices (0..15), named colors or RGBA values (0-1.0)
# Gradient = a color index, a named color or a list of RGBA values (0-1.0)
# D = degrees (not scaled)
# R, MinR, MaxR = radius (scaled number)
# Loops = non-scaled number

(to rectangle (SizeX SizeY LR Colors) # LR = turn direction, optional: Colors, one per vertex
   (default LR right)
   (rep 2
      (forward SizeX (++ Colors))
      (LR 90.0)
      (forward SizeY (++ Colors))
      (LR 90.0) ) )

(to square (Size) (rectangle Size Size))

(to solidrectangle (SizeX SizeY)
   (filled (rectangle SizeX SizeY)) )

(to solidsquare (Size) (filled (rectangle Size Size)))

(to gradientrectangle (SizeX SizeY Gradient)
   (filled (rectangle SizeX SizeY NIL
      (list Gradient Gradient (; *Window fore)) ) ) )

(to gradientsquare (Size Gradient)
   (gradientrectangle Size Size Gradient) )
   
(to ngon (Size Sides LR Colors) # LR = turn direction, optional: Colors, one per vertex
   (let A (/ 360.0 Sides)
      (default LR right)
      (rep Sides
         (forward Size (++ Colors))
         (LR A) ) ) )

(to solidngon (Size Sides LR) (filled (ngon Size Sides LR)))

(to triangle (Size LR) (ngon Size 3 LR))

(to solidtriangle (Size LR) (solidngon Size 3 LR))

(to arc (D R LR) # D = degrees (not scaled), R = radius (scaled), LR = turn direction
   (default LR left)
   (let S (/ (*/ 2 pi R 1.0) 360)
      (rep D
         (forward S)
         (LR 1.0) ) ) )

(to centerarc (D R LR)
   (default LR left)
   (penup)
   (forward R)
   (LR 90.0)
   (pendown)
   (forward 0)
   (arc D R LR)
   (LR 270.0)
   (penup)
   (back R) )

(to solidcenterarc (D R LR) (filled (centerarc D R LR)))

(to circle (R LR) (arc 360 R LR))

(to solidcircle (R LR) (filled (circle R LR)))

(to centercircle (R)
   (penup)
   (forward R)
   (left 90.0)
   (pendown)
   (circle R left)
   (penup)
   (right 90.0)
   (back R) )

(to solidcentercircle (R)
   (penup)
   (forward R)
   (left 90.0)
   (pendown)
   (filled (circle R left))
   (penup)
   (right 90.0)
   (back R) )
   
(to spiral (MinR MaxR Loops LR)
   (let D (/ (- MaxR MinR) (* Loops 2))
      (until (> MinR MaxR)
         (arc 180 MinR LR)
         (inc 'MinR D) ) ) )
         
(to dash (N Width)
   (rep N
      (pendown)
      (forward Width)
      (penup)
      (forward Width) ) )

# Define short equivalents for some of the functions

(def 'fw forward)
(def 'bk back)
(def 'rt right)
(def 'lt left)
(def 'pu penup)
(def 'pd pendown)
(def 'st showturtle)
(def 'ht hideturtle)
(def 'pc setpencolor)
(def 'sc setbackground)
(def 'cs clearscreen)

# Debug info
`*Dbg

(mapc '((S) (put S 'doc `(pack (car (file)) "res/commands.html")))
   '(fw bk rt lt pu pd st ht pc sc cs showturtle hideturtle setturtlesize
     icon home left right setheading towards goto jumpto clean clearscreen
     floodfill rep filled label rectangle square solidrectangle
     gradientrectangle gradientsquare ngon solidngon triangle solidtriangle
     arc centerarc solidcenterarc circle solidcircle centercircle
     solidcentercircle spiral dash
     rgba pencolor setpencolor background setbackground tint setpenalpha
     startturtle drawfb endturtle iconfromfile iconfit
     iconfromscreen savefromscreen destroyicon drawturtle setlabelheight
     setlabelfont resizewindow smoothon smoothoff reset penup pendown
     setpenwidth heading degrees down? loop0 loop+ loop- loop? repaint
     xcor ycor pos poly? to showturtle? turtlesize forward back setx sety
     pget pset visible? *Colors penalpha ) )
